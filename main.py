from bitcoin.rpc import RawProxy 
from time import sleep


def main():
    address = '3DmaWZRUCb1uWy7msFDGL98idGUZHRq6SS'

    p = RawProxy()
    i = 0
    while i < p.getblockcount():
        previous_block = p.getblockcount() - i
        print(previous_block)
        sleep(1)
        block_data = get_block(get_blockhash(previous_block))
        block_tx_ids = get_block_transactions_ids(block_data)
        for txid in block_tx_ids:
            tx_info = decode_raw_transaction(get_raw_transaction(txid))
            #print(tx_info)
            for output in tx_info['vout']:
                if 'addresses' not in output['scriptPubKey'].keys() :
                   pass
                else:
                    #print(output['scriptPubKey'].keys())
                    output_address = output['scriptPubKey']['addresses'][0]
                    if address == output_address:
                        tx_id = tx_info['txid']
                        outputvalue_1 = output['value']
                        print("txid:",tx_id, "\noutput value:", outputvalue_1, "\naddress:", output_address)
                        return 1
                        
            #print(type(tx_info['vout'][0]['scriptPubKey']['addresses']))
            #print(tx_info['vout'][0]['scriptPubKey']['addresses'])
            #print(tx_info['vout'][0]['scriptPubKey']['addresses'].keys())

            #sleep(5)
        #print(get_block_transactions_ids(block_data))
        i += 1
        #print("-----")
        #print(previous_block)

        #sleep(2)
    info = p.getblock(p.getblockhash(p.getblockcount()))

    #for i in info['tx']:
     #   print(i)

def decode_raw_transaction(raw_transaction):
    return RawProxy().decoderawtransaction(raw_transaction)

def get_raw_transaction(tx_id):
    return RawProxy().getrawtransaction(tx_id)
    
def get_blockhash(block_height):
    return RawProxy().getblockhash(block_height)

def get_block(block_hash):
    return RawProxy().getblock(block_hash)

def get_block_transactions_ids(block_info):
    txid_list = []
    for i in block_info['tx']:
        txid_list.append(i)
    return txid_list

def get_tx_info(tx_id):
    return RawProxy().decoderawtransaction(RawProxy().getrawtransaction(tx_id))

def get_input_txid(tx_id):
    return (RawProxy().decoderawtransaction(RawProxy().getrawtransaction(tx_id))['vin'])

if __name__== "__main__":
      main()
